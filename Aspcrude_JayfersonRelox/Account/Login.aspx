﻿<%@ Page Title="Log In" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="Aspcrude_JayfersonRelox.Account.Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
   
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
 
      <div class="card-deck mb-3 text-left">
        <div class="card box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal"> Log In</h4>
          </div>
          <div class="card-body">
            
             
    <p>
        Please enter your username and password.
        <asp:HyperLink ID="RegisterHyperLink" runat="server" EnableViewState="false">Register</asp:HyperLink> if you don't have an account.
    </p>
    <asp:Login ID="LoginUser" runat="server" EnableViewState="false" RenderOuterTable="false">
        <LayoutTemplate>
            <span class="failureNotification">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification" 
                 ValidationGroup="LoginUserValidationGroup"/>
            <div class="accountInfo">
                <fieldset class="login">
                    <legend>Account Information</legend>
                    <p>
                   
                    <div class="row">
                     <div class="col-md-4 mb-3">
                
               
                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Username:</asp:Label>
                        <asp:TextBox ID="UserName" runat="server" class="form-control" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                             CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                             </div>
                             </div>
                           
                    </p>
                    <p>
                    
                
                       <div class="row">
                     <div class="col-md-4 mb-3">
                        <asp:Label  ID="PasswordLabel"   runat="server" AssociatedControlID="Password">Password:</asp:Label>
                        <asp:TextBox  ID="Password"    runat="server" class="form-control" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                             CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                             </div>
                             </div>
                          
                    </p>
                    <p>
                    <div class="row">
                     <div class="col-md-4 mb-3">
                        <asp:CheckBox ID="RememberMe" runat="server"/>
                        <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="inline">Keep me logged in</asp:Label>
                    </p>
                        </div>
                </fieldset>
                <p class="submitButton">
         <asp:Button class="btn btn-primary " ID="LoginButton" runat="server" CommandName="Login"  Text="Log In" ValidationGroup="LoginUserValidationGroup"/>
                 
                </p>
          
            </div>
        </LayoutTemplate>
    </asp:Login>
    </div>
    </div>
    </div>
</asp:Content>
